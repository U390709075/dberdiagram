-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `anime`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `anime` ;

CREATE TABLE IF NOT EXISTS `anime` (
  `anime_id` INT NOT NULL,
  `name` VARCHAR(150) NOT NULL,
  `score` FLOAT NOT NULL,
  `type` VARCHAR(15) NOT NULL,
  `episodes` SMALLINT NOT NULL,
  `date` DATE NOT NULL,
  `continues` TINYINT NOT NULL,
  `duration` SMALLINT NOT NULL,
  `ranked` INT NOT NULL,
  `popularity_rank` INT NOT NULL,
  `members` INT NOT NULL,
  `favorites` INT NOT NULL,
  `watching` INT NOT NULL,
  `completed` INT NOT NULL,
  `on_hold` INT NOT NULL,
  `dropped` INT NOT NULL,
  `planto_watch` INT NOT NULL,
  PRIMARY KEY (`anime_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `genres`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `genres` ;

CREATE TABLE IF NOT EXISTS `genres` (
  `anime_id` INT NOT NULL,
  `genre_name` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`anime_id`, `genre_name`),
  CONSTRAINT `fk_genres_Animes`
    FOREIGN KEY (`anime_id`)
    REFERENCES `anime` (`anime_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_genres_Animes_idx` ON `genres` (`anime_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `producers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `producers` ;

CREATE TABLE IF NOT EXISTS `producers` (
  `anime_id` INT NOT NULL,
  `producer_name` VARCHAR(75) NOT NULL,
  PRIMARY KEY (`anime_id`, `producer_name`),
  CONSTRAINT `fk_producers_animes1`
    FOREIGN KEY (`anime_id`)
    REFERENCES `anime` (`anime_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_producers_animes1_idx` ON `producers` (`anime_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `licenscors`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `licenscors` ;

CREATE TABLE IF NOT EXISTS `licenscors` (
  `anime_id` INT NOT NULL,
  `licenscor_name` VARCHAR(55) NOT NULL,
  PRIMARY KEY (`anime_id`, `licenscor_name`),
  CONSTRAINT `fk_licenscors_animes1`
    FOREIGN KEY (`anime_id`)
    REFERENCES `anime` (`anime_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_licenscors_animes1_idx` ON `licenscors` (`anime_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `scores`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `scores` ;

CREATE TABLE IF NOT EXISTS `scores` (
  `anime_id` INT NOT NULL,
  `score_1` INT NOT NULL,
  `score_2` INT NOT NULL,
  `score_3` INT NOT NULL,
  `score_4` INT NOT NULL,
  `score_5` INT NOT NULL,
  `score_6` INT NOT NULL,
  `score_7` INT NOT NULL,
  `score_8` INT NOT NULL,
  PRIMARY KEY (`anime_id`),
  CONSTRAINT `fk_scores_anime1`
    FOREIGN KEY (`anime_id`)
    REFERENCES `anime` (`anime_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `source`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `source` ;

CREATE TABLE IF NOT EXISTS `source` (
  `animes_anime_id` INT NOT NULL,
  `source_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`animes_anime_id`, `source_name`),
  CONSTRAINT `fk_source_animes1`
    FOREIGN KEY (`animes_anime_id`)
    REFERENCES `anime` (`anime_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_source_animes1_idx` ON `source` (`animes_anime_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `studios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `studios` ;

CREATE TABLE IF NOT EXISTS `studios` (
  `anime_id` INT NOT NULL,
  `studio_name` VARCHAR(75) NOT NULL,
  PRIMARY KEY (`anime_id`, `studio_name`),
  CONSTRAINT `fk_studios_anime1`
    FOREIGN KEY (`anime_id`)
    REFERENCES `anime` (`anime_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_studios_anime1_idx` ON `studios` (`anime_id` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `anime`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `anime` (`anime_id`, `name`, `score`, `type`, `episodes`, `date`, `continues`, `duration`, `ranked`, `popularity_rank`, `members`, `favorites`, `watching`, `completed`, `on_hold`, `dropped`, `planto_watch`) VALUES (1, 'Cowboy Bebop', 8.78, 'TV', 26, '9/18/2015', True, 24, 28, 39, 1251960, 61971, 105808, 718161, 71513, 26678, 329800);
INSERT INTO `anime` (`anime_id`, `name`, `score`, `type`, `episodes`, `date`, `continues`, `duration`, `ranked`, `popularity_rank`, `members`, `favorites`, `watching`, `completed`, `on_hold`, `dropped`, `planto_watch`) VALUES (2, 'Cowboy Bebop Tengoku no Tobira', 8.39, 'Movie', 1, '7/30/2016', False, 115, 159, 518, 273145, 1174, 4143, 208333, 1935, 770, 57964);

COMMIT;


-- -----------------------------------------------------
-- Data for table `genres`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `genres` (`anime_id`, `genre_name`) VALUES (1, 'action');
INSERT INTO `genres` (`anime_id`, `genre_name`) VALUES (2, 'mystery');

COMMIT;


-- -----------------------------------------------------
-- Data for table `producers`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `producers` (`anime_id`, `producer_name`) VALUES (1, 'Bandai Original');
INSERT INTO `producers` (`anime_id`, `producer_name`) VALUES (2, 'Sunrise');

COMMIT;


-- -----------------------------------------------------
-- Data for table `licenscors`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `licenscors` (`anime_id`, `licenscor_name`) VALUES (1, 'Funimation');
INSERT INTO `licenscors` (`anime_id`, `licenscor_name`) VALUES (2, 'Sony Pictures Entertainment');

COMMIT;


-- -----------------------------------------------------
-- Data for table `scores`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `scores` (`anime_id`, `score_1`, `score_2`, `score_3`, `score_4`, `score_5`, `score_6`, `score_7`, `score_8`) VALUES (1, 1357, 741, 1580, 229170, 182126, 131625, 62330, 20688);
INSERT INTO `scores` (`anime_id`, `score_1`, `score_2`, `score_3`, `score_4`, `score_5`, `score_6`, `score_7`, `score_8`) VALUES (2, 221, 109, 379, 30043, 49201, 49505, 22632, 5805);

COMMIT;


-- -----------------------------------------------------
-- Data for table `source`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `source` (`animes_anime_id`, `source_name`) VALUES (1, 'Original');
INSERT INTO `source` (`animes_anime_id`, `source_name`) VALUES (2, 'Original');

COMMIT;


-- -----------------------------------------------------
-- Data for table `studios`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `studios` (`anime_id`, `studio_name`) VALUES (1, 'Sunrise');
INSERT INTO `studios` (`anime_id`, `studio_name`) VALUES (2, 'Bones');

COMMIT;

